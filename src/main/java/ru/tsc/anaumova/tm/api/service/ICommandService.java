package ru.tsc.anaumova.tm.api.service;

import ru.tsc.anaumova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}