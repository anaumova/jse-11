package ru.tsc.anaumova.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void clearTaskList();

    void createTask();

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskByIndex();

    void removeTaskById();

    void showTaskByIndex();

    void showTaskById();

}